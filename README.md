```
 d8b          888                       888                             d8b 
 Y8P          888                       888                             Y8P 
              888                       888                                 
 888 88888b.  888888 888  888       .d88888  .d88b.       88888b.d88b.  888 
 888 888 "88b 888    888  888      d88" 888 d88""88b      888 "888 "88b 888 
 888 888  888 888    Y88  88P      888  888 888  888      888  888  888 888 
 888 888 d88P Y88b.   Y8bd8P       Y88b 888 Y88..88P      888  888  888 888 
 888 88888P"   "Y888   Y88P         "Y88888  "Y88P"       888  888  888 888 
     888                                                                    
     888                                                                    
     888   
```

*Esta lista IPTV é montada com canais de interesse pessoal do mantenedor, mas acessível para todas as pessoas.  
Todos os canais desta lista IPTV estão abertos e disponíveis na internet, apenas foram reunidos em um arquivo M3U.*

### Mantenedor

- Contato pelo Telegram: [@technoclaw](https://t.me/technoclaw)  

### Atualizações

- 03/10/2024: 284 canais.

## :tv: Como usar esta lista

<details open>
<summary><h3>URL do arquivo M3U</h3></summary>

Curto: `https://is.gd/iptvmi`  
Original: `https://git.disroot.org/technoclaw/IPTV-do-Mi/raw/branch/main/canais.m3u`  
</details>

<details>
<summary><h3>Aplicativos recomendados</h3></summary>

<details>
<summary><b>yuki-iptv (Linux)</b></summary>

https://codeberg.org/liya/yuki-iptv

1. Na janela "Playlist", clique em "Add"
2. Escolha um nome para a sua playlist em "Name" e cole o link `https://is.gd/iptvmi` em "M3U/XSPF playlist"
3. Depois, salve clicando em "Save"

Para começar a assistir os canais, clique 2x na playlist que você adicionou.

Dica: para esconder os controles do MPV, vá em "Options > Settings > Other" e marque "Hide mpv panel".
</details>

<details>
<summary><b>IPTVNator (Windows, macOS & Linux)</b></summary>

https://github.com/4gray/iptvnator

1. Clique em "ADD VIA URL"
2. Cole o link do arquivo M3U: `https://is.gd/iptvmi`
3. Depois, salve clicando em "ADD PLAYLIST"

Para começar a assistir os canais, clique na playlist que você adicionou.

</details>

<details>
<summary><b>Televizo (Android)</b></summary>

https://televizo.net

1. Clique em "Criar lista de reprodução" e escolha "Nova lista de reprodução M3U"
2. Escolha um nome para a lista de reprodução e adicione o link para a lista: `https://is.gd/iptvmi`
3. Depois, salve clicando em ":heavy_check_mark:"
</details>

<details>
<summary><b>TVirl (Android TV)</b></summary>

https://play.google.com/store/apps/details?id=by.stari4ek.tvirl

1. Ao abrir o aplicativo, clique em "Canais > TVirl"
2. Digite em "URL da lista" o link do arquivo M3U: `https://is.gd/iptvmi`
3. Depois, salve clicando em "Continuar > Instalar > Concluído"

Para começar a assistir os canais, use o Live Channels da Google.
</details>

</details>


## :1234: IDs e categorias

1-xxx: *Regional*  
2-xxx: *Notícias*  
3-xxx: *Estilo de Vida*  
4-xxx: *Curiosidades*  
5-xxx: *Entretenimento*  
6-xxx: *Séries & Filmes*  
7-xxx: *Retrô*  
8-xxx: *Infantil & Teen*  
9-xxx: *Anime & Geek*  
10-xxx: *Esportes*  
11-xxx: *Rural*  
12-xxx: *Música*  
